# CMake entry point
cmake_minimum_required (VERSION 3.0)
project (astroDucks)

find_package(OpenGL REQUIRED)

if( CMAKE_BINARY_DIR STREQUAL CMAKE_SOURCE_DIR )
    message( FATAL_ERROR "Please select another Build Directory" )
endif()
if( CMAKE_SOURCE_DIR MATCHES " " )
	message( "Your Source Directory contains spaces. If you experience problems when compiling, this can be the cause." )
endif()
if( CMAKE_BINARY_DIR MATCHES " " )
	message( "Your Build Directory contains spaces. If you experience problems when compiling, this can be the cause." )
endif()

# Compile external dependencies
add_subdirectory (external)

if(INCLUDE_DISTRIB)
	add_subdirectory(distrib)
endif(INCLUDE_DISTRIB)

include_directories(
	external/glfw-3.1.2/include/
	external/glm-0.9.7.1/
	external/glew-1.13.0/include/
    external/irrKlang_1_6_0/include/
    ${OPENGL_INCLUDE_DIRS}
	.
)

link_directories(
)    

set(ALL_LIBS
	${OPENGL_LIBRARY}
	glfw
	GLEW_1130
)

add_definitions(
	-DTW_STATIC
	-DTW_NO_LIB_PRAGMA
	-DTW_NO_DIRECT3D
	-DGLEW_STATIC
	-D_CRT_SECURE_NO_WARNINGS
	-std=c++14
	-O3
)

# testQuadTree
add_executable(testQuadTree
	src/mainTestQuadTree.cpp
    src/initGL.cpp
    src/initGL.h
	src/quadTree.cpp
	src/quadTree.h	
	src/particleSystem.cpp
	src/particleSystem.h	
	src/collisionDetection.cpp
	src/collisionDetection.h
	src/aStarPathFind.cpp
	src/aStarPathFind.h
	src/sphere.cpp
	src/sphere.h	
    src/handleInput.cpp
    src/handleInput.h        
    src/model3d.cpp
    src/model3d.h
    src/duck.cpp
    src/duck.h        
    src/player.cpp
    src/player.h	
    src/projectileBall.cpp
    src/projectileBall.h	
    src/gameScene.cpp
    src/gameScene.h    
    src/shader.cpp
	src/shader.h
	src/objloader.cpp
	src/objloader.h
	src/collisionSystem.cpp
	src/collisionSystem.h
      
	src/model3d.fragmentshader
	src/model3d.vertexshader
	src/particle.fragmentshader
	src/particle.vertexshader
	
)
target_link_libraries(testQuadTree
	${ALL_LIBS}
    
)
if (NOT ${CMAKE_GENERATOR} MATCHES "Xcode" )
add_custom_command(
   TARGET testQuadTree POST_BUILD
   COMMAND ${CMAKE_COMMAND} -E copy "${CMAKE_CURRENT_BINARY_DIR}/${CMAKE_CFG_INTDIR}/testQuadTree${CMAKE_EXECUTABLE_SUFFIX}" "${CMAKE_CURRENT_SOURCE_DIR}/bin"
)
elseif (${CMAKE_GENERATOR} MATCHES "Xcode" )

endif (NOT ${CMAKE_GENERATOR} MATCHES "Xcode" )

add_executable(astroDucks 
	src/main.cpp
    src/initGL.cpp
    src/initGL.h
	src/quadTree.cpp
	src/quadTree.h	
	src/particleSystem.cpp
	src/particleSystem.h	
	src/collisionDetection.cpp
	src/collisionDetection.h
	src/aStarPathFind.cpp
	src/aStarPathFind.h
	src/sphere.cpp
	src/sphere.h	
    src/handleInput.cpp
    src/handleInput.h        
    src/model3d.cpp
    src/model3d.h
    src/model3d_cooktorrance.cpp
    src/model3d_cooktorrance.h    
    src/duck.cpp
    src/duck.h        
    src/player.cpp
    src/player.h	
    src/projectileBall.cpp
    src/projectileBall.h	
    src/gameScene.cpp
    src/gameScene.h    
    src/shader.cpp
	src/shader.h
	src/objloader.cpp
	src/objloader.h
	src/collisionSystem.cpp
	src/collisionSystem.h       	
	src/gui.cpp
	src/gui.h
	src/texture.cpp
	src/texture.h
	src/texturedQuad.cpp
	src/texturedQuad.h
	src/texturedQuad3D.cpp
	src/texturedQuad3D.h    
	src/text2D.cpp
	src/text2D.h
	src/lightData.cpp
	src/lightData.h    
	src/inGameHUD.cpp
	src/inGameHUD.h
	src/renderToTexture.cpp
	src/renderToTexture.h
	src/waterGfxEffect.cpp
	src/waterGfxEffect.h
	src/poolWalls.cpp
	src/poolWalls.h
    src/renderStruct.h
    src/gameDefs.h
    src/gameDefs.cpp
)
target_link_libraries(astroDucks
	${ALL_LIBS}
)
if (NOT ${CMAKE_GENERATOR} MATCHES "Xcode" )
add_custom_command(
   TARGET astroDucks POST_BUILD
   COMMAND ${CMAKE_COMMAND} -E copy "${CMAKE_CURRENT_BINARY_DIR}/${CMAKE_CFG_INTDIR}/astroDucks${CMAKE_EXECUTABLE_SUFFIX}" "${CMAKE_CURRENT_SOURCE_DIR}/bin"
   COMMAND ${CMAKE_COMMAND} -E copy "${CMAKE_CURRENT_LIST_DIR}/src/model3d.fragmentshader" "${CMAKE_CURRENT_SOURCE_DIR}/bin"
   COMMAND ${CMAKE_COMMAND} -E copy "${CMAKE_CURRENT_LIST_DIR}/src/model3d.vertexshader" "${CMAKE_CURRENT_SOURCE_DIR}/bin"
   COMMAND ${CMAKE_COMMAND} -E copy "${CMAKE_CURRENT_LIST_DIR}/src/model3d_cooktorrance.fragmentshader" "${CMAKE_CURRENT_SOURCE_DIR}/bin"
   COMMAND ${CMAKE_COMMAND} -E copy "${CMAKE_CURRENT_LIST_DIR}/src/model3d_cooktorrance.vertexshader" "${CMAKE_CURRENT_SOURCE_DIR}/bin" 
   COMMAND ${CMAKE_COMMAND} -E copy "${CMAKE_CURRENT_LIST_DIR}/src/particle.fragmentshader" "${CMAKE_CURRENT_SOURCE_DIR}/bin"
   COMMAND ${CMAKE_COMMAND} -E copy "${CMAKE_CURRENT_LIST_DIR}/src/particle.vertexshader" "${CMAKE_CURRENT_SOURCE_DIR}/bin"
   COMMAND ${CMAKE_COMMAND} -E copy "${CMAKE_CURRENT_LIST_DIR}/src/texturedQuad.fragmentshader" "${CMAKE_CURRENT_SOURCE_DIR}/bin"
   COMMAND ${CMAKE_COMMAND} -E copy "${CMAKE_CURRENT_LIST_DIR}/src/texturedQuad.vertexshader" "${CMAKE_CURRENT_SOURCE_DIR}/bin"
   COMMAND ${CMAKE_COMMAND} -E copy "${CMAKE_CURRENT_LIST_DIR}/src/texturedQuadSimpleLight.fragmentshader" "${CMAKE_CURRENT_SOURCE_DIR}/bin"
   COMMAND ${CMAKE_COMMAND} -E copy "${CMAKE_CURRENT_LIST_DIR}/src/texturedQuadSimpleLight.vertexshader" "${CMAKE_CURRENT_SOURCE_DIR}/bin"
   COMMAND ${CMAKE_COMMAND} -E copy "${CMAKE_CURRENT_LIST_DIR}/src/texturedQuadTransparent.fragmentshader" "${CMAKE_CURRENT_SOURCE_DIR}/bin"
   COMMAND ${CMAKE_COMMAND} -E copy "${CMAKE_CURRENT_LIST_DIR}/src/texturedQuadTransparent.vertexshader" "${CMAKE_CURRENT_SOURCE_DIR}/bin"   
   COMMAND ${CMAKE_COMMAND} -E copy "${CMAKE_CURRENT_LIST_DIR}/src/text.fragmentshader" "${CMAKE_CURRENT_SOURCE_DIR}/bin"
   COMMAND ${CMAKE_COMMAND} -E copy "${CMAKE_CURRENT_LIST_DIR}/src/text.vertexshader" "${CMAKE_CURRENT_SOURCE_DIR}/bin"
   
)
elseif (${CMAKE_GENERATOR} MATCHES "Xcode" )

endif (NOT ${CMAKE_GENERATOR} MATCHES "Xcode" )

add_executable(pbrMdlInspector 
	src/inspector/main.cpp
    src/initGL.cpp
    src/initGL.h
    src/model3d_cooktorrance.cpp
    src/model3d_cooktorrance.h    
    src/shader.cpp
	src/shader.h
	src/objloader.cpp
	src/objloader.h
	src/texture.cpp
	src/texture.h
	src/lightData.cpp
	src/lightData.h
	src/text2D.cpp
	src/text2D.h   
    src/renderStruct.h
  
)
target_link_libraries(pbrMdlInspector
	${ALL_LIBS}
)
if (NOT ${CMAKE_GENERATOR} MATCHES "Xcode" )
add_custom_command(
   TARGET pbrMdlInspector POST_BUILD
   COMMAND ${CMAKE_COMMAND} -E copy "${CMAKE_CURRENT_BINARY_DIR}/${CMAKE_CFG_INTDIR}/pbrMdlInspector${CMAKE_EXECUTABLE_SUFFIX}" "${CMAKE_CURRENT_SOURCE_DIR}/bin"
   COMMAND ${CMAKE_COMMAND} -E copy "${CMAKE_CURRENT_LIST_DIR}/src/model3d_cooktorrance.fragmentshader" "${CMAKE_CURRENT_SOURCE_DIR}/bin"
   COMMAND ${CMAKE_COMMAND} -E copy "${CMAKE_CURRENT_LIST_DIR}/src/model3d_cooktorrance.vertexshader" "${CMAKE_CURRENT_SOURCE_DIR}/bin" 
   COMMAND ${CMAKE_COMMAND} -E copy "${CMAKE_CURRENT_LIST_DIR}/src/text.fragmentshader" "${CMAKE_CURRENT_SOURCE_DIR}/bin"
   COMMAND ${CMAKE_COMMAND} -E copy "${CMAKE_CURRENT_LIST_DIR}/src/text.vertexshader" "${CMAKE_CURRENT_SOURCE_DIR}/bin"   
)
elseif (${CMAKE_GENERATOR} MATCHES "Xcode" )

endif (NOT ${CMAKE_GENERATOR} MATCHES "Xcode" )