#ifndef ASTARPATHFIND_H
#define ASTARPATHFIND_H

// Forward declarations

#include <memory>
#include <vector>

#define XSTEP 1.0f
#define YSTEP 1.0f
#define MAX_STEP_X 11
#define MAX_STEP_Y 8


class NodeAStar
{
	public:

    int 		m_x;
    int 		m_y;
	NodeAStar*	m_parent;
    int 		m_gCost;
    int 		m_hCost; 
    int 		m_fCost;
	bool		m_traversable;

	static void InitAStarPathFind();



	/*
		
		----------------------------> X
		|0, 0|1, 0|
		|0, 1|1, 1|
		|0, 2|1, 2|
		|0, 3|1, 3|
		|0, 4|1, 4|
		|0, 5|1, 5|
		|0, 6|1, 6|
		|0, 7|1, 7|		
		Y
		+
	*/
	static std::vector<std::unique_ptr<NodeAStar> > m_worldMap;
};



void findPath(NodeAStar* startNode, NodeAStar* goalNode, std::vector<NodeAStar*> path);

#endif