#ifndef GAMESCENE_H
#define GAMESCENE_H

// Forward declarations
class Duck;
class Player;
class ParticleSystem;
class HUDGUI;
class PoolWalls;
class WaterGfxEffect;
class GameDefData;


#include <memory>
#include <vector>
#include "lightData.h"

class GameScene
{
	public:
	GameScene(std::vector<Duck* > &ducks, std::vector<std::unique_ptr<Player> > &players,
				std::vector<ParticleSystem* > &particleSystems, HUDGUI* hudGUI, PoolWalls* poolWalls,
				WaterGfxEffect &waterGfxEffect, GameDefData& gameDefData, int windowWidth, int windowHeight);
	~GameScene();

	void PrepareForRender();
	void RenderScene() const;

	void RecalculateWaterMatrices();
		
	void RenderSceneWithoutHUD(const glm::mat4 &view, const glm::mat4 &projection, bool renderPoolWalls,
								bool renderParticleEffects, bool invertLightPos) const;

	const glm::mat4& PlayerViewMatrix() const;
	const glm::mat4& WaterViewMatrix() const;

	const glm::mat4& PlayerProjectionMatrix() const;
	const glm::mat4& WaterProjectionMatrix() const;

	private:

	void RenderScene(const glm::mat4 &view, const glm::mat4 &projection) const;
	void RenderModel3DObjects(const glm::mat4 &view, const glm::mat4 &projection, bool invertLightPos) const;

	glm::mat4	m_projection;
	glm::mat4	m_view;

	glm::mat4	m_projectionWaterCameraReflection;
	glm::mat4	m_viewWaterCameraReflection;

	glm::mat4	m_hudOrthoMVP;	
	int			m_windowWidth, m_windowHeight;

	glm::vec4	m_clipPlane; // Used for rendering water reflection

	glm::vec3	m_backgroundColor;

	std::vector<Duck* > &m_ducks;
	std::vector<std::unique_ptr<Player> > &m_players;
	std::vector<ParticleSystem* > &m_particleSystems;

	PoolWalls*						m_poolWalls;

	HUDGUI*							m_hudGUI;

	SceneLightData					m_sceneLight;

	WaterGfxEffect&					m_waterGfxEffect;
	GameDefData&					m_gameDefData;				
	
};

#endif