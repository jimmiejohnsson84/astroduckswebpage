#ifndef PARTICLESYSTEM_H
#define PARTICLESYSTEM_H

#include <vector>
#include <memory>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>


/*
	Uses instancing to handle a large amount of particles.	
*/	
class ParticleSystem
{
	public:

	static int maxParticlesTotal;
	static int maxParticlesPerSystem;

	class Particle
	{
		public:
		Particle();
		~Particle();

		void CreateParticle(float *pos, float *moveDir, unsigned char *color, float size);

		union ParticleData
		{
			struct activeData
			{
				float m_pos[3];
				float m_moveDir[3];
				unsigned char m_color[4];
				float m_size;				
			};

			activeData m_active;
			Particle* m_nextFree;
		};

		ParticleData m_currentState;
		float m_life;
	};	

	class ParticlePool
	{
		public:
		static ParticlePool& GetInstance();

		~ParticlePool();

		Particle*	CreateParticle(float *pos, float *moveDir, unsigned char *color, float size);
		void		ReturnParticleToFreePool(Particle* p);

		private:

		ParticlePool();

		Particle* m_firstFree;

		std::vector<Particle> m_particles;
	};
	
	ParticleSystem();
	~ParticleSystem();

	void CreateExplosion(int power, int nrParticles, glm::vec3 centerPos, float dieFactor, float slowDownFactor, unsigned char *color, float size);
	void SetSortParticles(bool sortParticles);
	
	bool Tick(float deltaTime);
	void Render(const glm::mat4 &projection, const glm::mat4 &view);

	static void InitParticleSystem();
	static void PrepareRender();
	static void AfterRender();	

	static void TickParticleSystems(float deltaTime);
	static void AddParticleSystem(ParticleSystem* particleSystem);
	static std::vector<ParticleSystem* >& GetParticleSystem();

	private:

	void SlowParticleDown(float &v, float &slowDownFactor);

	float									m_particleSpeedSlowdownFactor;
	float									m_particleDieFactor;
	bool									m_sortParticles;	

	std::vector<Particle*> m_particles;
	std::vector<GLfloat> 					m_particlesPositionAndSizeData;
	std::vector<GLubyte>					m_particlesColorData;


	ParticlePool							&m_particlePool;

	// Shader handles	
	static GLuint m_CameraRight_worldspaceID;
	static GLuint m_CameraUp_worldspaceID;
	static GLuint m_VPID;
	static GLuint m_programID;

	// Buffer Objects
	GLuint m_particlesPositionAndSizeBuffer;
	GLuint m_particlesColorBuffer;

	
	static GLuint m_particleVBO;

	static std::vector<ParticleSystem* > m_particleSystems;
};

#endif