#ifndef WATER_GFX_EFFECT_H
#define WATER_GFX_EFFECT_H


// Fwds
class GameScene;
class TexturedQuad3D;
class GameDefData;
struct RenderStateTexturedQuad3D;

class WaterGfxEffect
{
    public:
    WaterGfxEffect(const GameDefData& gameDefData);
    ~WaterGfxEffect();

    void CreateReflectionTexture(const GameScene* gameScene);

    void Tick();

    void Render(const RenderStateTexturedQuad3D& renderState, const glm::mat4 &projection, const glm::mat4 &view);

    private:    
    GLuint m_reflectionFrameBuffer;
    GLuint m_reflectionRenderTexture;
    GLuint m_reflectionDepthBuffer;

    TexturedQuad3D*     m_waterSurface;
    const GameDefData&  m_gameDefData;
    glm::vec3 	        m_pos;
};


#endif 