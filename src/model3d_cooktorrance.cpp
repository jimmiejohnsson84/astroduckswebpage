// Include standard headers
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <vector>
using namespace std;

// Include GLEW
#include <GL/glew.h>

// Include GLFW
#include <GLFW/glfw3.h>

// Include GLM
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
using namespace glm;

#include "objloader.h"
#include "model3d_cooktorrance.h"
#include "shader.h"
#include "lightData.h"
#include "renderStruct.h"

Model3dCookTorrance::shaderProgramHandles Model3dCookTorrance::m_model3DCookTorranceShaderHandles;

void Model3dCookTorrance::InitModel3dCookTorrance()
{
	m_model3DCookTorranceShaderHandles.m_programID = LoadShaders( "model3d_cooktorrance.vertexshader", "model3d_cooktorrance.fragmentshader" );	

	// Get handles for data in the shader
	m_model3DCookTorranceShaderHandles.m_modelViewProjectionID = glGetUniformLocation(m_model3DCookTorranceShaderHandles.m_programID, "MVP");
	m_model3DCookTorranceShaderHandles.m_viewMatrixID = glGetUniformLocation(m_model3DCookTorranceShaderHandles.m_programID, "V");
	m_model3DCookTorranceShaderHandles.m_modelMatrixID = glGetUniformLocation(m_model3DCookTorranceShaderHandles.m_programID, "M");
	m_model3DCookTorranceShaderHandles.m_modelViewMatrixID = glGetUniformLocation(m_model3DCookTorranceShaderHandles.m_programID, "MV");
	m_model3DCookTorranceShaderHandles.m_modelView3By3MatrixID = glGetUniformLocation(m_model3DCookTorranceShaderHandles.m_programID, "MV3By3");
	m_model3DCookTorranceShaderHandles.m_normalMatrixID = glGetUniformLocation(m_model3DCookTorranceShaderHandles.m_programID, "NM");
	m_model3DCookTorranceShaderHandles.m_lightPos_worldspaceID = glGetUniformLocation(m_model3DCookTorranceShaderHandles.m_programID, "lightPosition_worldspace");
	m_model3DCookTorranceShaderHandles.m_lightColorID = glGetUniformLocation(m_model3DCookTorranceShaderHandles.m_programID, "lightColor");
	m_model3DCookTorranceShaderHandles.m_lightIntensityID = glGetUniformLocation(m_model3DCookTorranceShaderHandles.m_programID, "lightIntensity");
	
	m_model3DCookTorranceShaderHandles.m_textureAlbedoID = glGetUniformLocation(m_model3DCookTorranceShaderHandles.m_programID, "textureAlbedo");
	m_model3DCookTorranceShaderHandles.m_textureNormalID = glGetUniformLocation(m_model3DCookTorranceShaderHandles.m_programID, "textureNormal");
	m_model3DCookTorranceShaderHandles.m_textureMetallicID = glGetUniformLocation(m_model3DCookTorranceShaderHandles.m_programID, "textureMetallic");
	m_model3DCookTorranceShaderHandles.m_textureRoughnessID = glGetUniformLocation(m_model3DCookTorranceShaderHandles.m_programID, "textureRoughness");
	m_model3DCookTorranceShaderHandles.m_textureAOID = glGetUniformLocation(m_model3DCookTorranceShaderHandles.m_programID, "textureAO");

	m_model3DCookTorranceShaderHandles.m_userClipPlane0ID = glGetUniformLocation(m_model3DCookTorranceShaderHandles.m_programID, "userClipPlane0");
	
}
//----------------------------------------------------------------------
void Model3dCookTorrance::PrepareRender()
{
	glUseProgram(m_model3DCookTorranceShaderHandles.m_programID);
	glEnableVertexAttribArray(0);
	glEnableVertexAttribArray(1);
	glEnableVertexAttribArray(2);
	glEnableVertexAttribArray(3);
}
//----------------------------------------------------------------------
void Model3dCookTorrance::AfterRender()
{
	glDisableVertexAttribArray(0);
	glDisableVertexAttribArray(1);		
	glDisableVertexAttribArray(2);
	glDisableVertexAttribArray(3);		
}


//----------------------------------------------------------------------
//----------------------------------------------------------------------
Model3dCookTorrance::Model3dCookTorrance()
{
	m_elementBuffer = 0;
	m_vertexBuffer = 0;
	m_normalBuffer = 0;
	m_indicesSize = 0;
	m_tangents = 0;
	m_vertexBufferSize = 0;
	m_built = false;
}
//----------------------------------------------------------------------
Model3dCookTorrance::~Model3dCookTorrance()
{
	if(m_built)
	{
		glDeleteBuffers(1, &m_vertexBuffer);	
		glDeleteBuffers(1, &m_normalBuffer);
		glDeleteBuffers(1, &m_uvCoords);		
		glDeleteBuffers(1, &m_tangents);
	}
}
//----------------------------------------------------------------------
// Returns true if v1 can be considered equal to v2
bool is_near(float v1, float v2)
{
	return fabs( v1-v2 ) < 0.001f;
}
//----------------------------------------------------------------------
// Searches through all already-exported vertices
// for a similar one.
// Similar = same position + same UVs + same normal
bool getSimilarVertexIndex(	glm::vec3 & in_vertex, 	glm::vec2 & in_uv, 	glm::vec3 & in_normal, 	std::vector<glm::vec3> & out_vertices,
							std::vector<glm::vec2> & out_uvs, std::vector<glm::vec3> & out_normals,	unsigned & result)
{
	// Lame linear search
	for ( unsigned int i=0; i<out_vertices.size(); i++ )
	{
		if (
			is_near( in_vertex.x , out_vertices[i].x ) &&
			is_near( in_vertex.y , out_vertices[i].y ) &&
			is_near( in_vertex.z , out_vertices[i].z ) &&
			is_near( in_uv.x     , out_uvs     [i].x ) &&
			is_near( in_uv.y     , out_uvs     [i].y ) &&
			is_near( in_normal.x , out_normals [i].x ) &&
			is_near( in_normal.y , out_normals [i].y ) &&
			is_near( in_normal.z , out_normals [i].z ))
		{
			result = i;
			return true;
		}
	}
	// No other vertex could be used instead.
	// Looks like we'll have to add it to the VBO.
	return false;
}
//----------------------------------------------------------------------
void indexVBO_TBN(	std::vector<glm::vec3> & in_vertices, std::vector<glm::vec2> & in_uvs, std::vector<glm::vec3> & in_normals,
					std::vector<glm::vec3> & in_tangents,
					
					std::vector<unsigned> & out_indices, std::vector<glm::vec3> & out_vertices, std::vector<glm::vec2> & out_uvs,
					std::vector<glm::vec3> & out_normals, std::vector<glm::vec3> & out_tangents)
{
	for ( unsigned int i=0; i<in_vertices.size(); i++ )
	{
		// Try to find a similar vertex in out_XXXX
		unsigned index;
		bool found = getSimilarVertexIndex(	in_vertices[i], in_uvs[i], in_normals[i], 
											out_vertices, out_uvs, out_normals, index);

		// A similar vertex is already in the VBO, use it instead !
		if ( found )
		{ 
			out_indices.push_back( index );

			// Average the tangents
			out_tangents[index] += in_tangents[i];
		}
		else // If not, it needs to be added in the output data.
		{ 
			out_vertices.push_back( in_vertices[i]);
			out_uvs.push_back( in_uvs[i]);
			out_normals.push_back( in_normals[i]);
			out_tangents.push_back( in_tangents[i]);
			out_indices.push_back( (unsigned)out_vertices.size() - 1 );
		}
	}
}
//----------------------------------------------------------------------
void Model3dCookTorrance::Build(const char *modelPath)
 {
	std::vector<glm::vec3> vertices;
	std::vector<glm::vec2> uvs;
	std::vector<glm::vec3> normals;

	std::vector<glm::vec3> tangents;
	std::vector<glm::vec3> bitangents;

	bool loadStatus = loadOBJ(modelPath, vertices, uvs, normals);
	if(loadStatus)
	{
		// Construct tangent & bitangent data for normal mapping
		// Assumes we are using only triangles, we work on each triangle to create the tangent & bitanget
		bool uvDirSet = false;
		float posXDir = 0.0f;


		for(int i = 0; i < vertices.size(); i+= 3)
		{
				glm::vec3& v0 = vertices[i + 0];
				glm::vec3& v1 = vertices[i + 1];
				glm::vec3& v2 = vertices[i + 2];

				glm::vec2& uv0 = uvs[i + 0];
				glm::vec2& uv1 = uvs[i + 1];
				glm::vec2& uv2 = uvs[i + 2];

				// Delta in model space				
				glm::vec3 deltaPos1 = v1 - v0;
				glm::vec3 deltaPos2 = v2 - v0;
	

				// Delta in "texture coordinate space"
				glm::vec2 deltaUV1 = uv1 - uv0;
				glm::vec2 deltaUV2 = uv2 - uv0;

				/* 	Time to calculate tangent & bitanget
					We could theoretically choose any tangent & bitanget given triangle (v1 - v0, v2 - v0), but we will
					chose them with respect to both orientation of the triangle and the uv delta

					There is a bit of matrix math involved in calculating this stuff - look into the details at a later stage.
				*/
				float f = 1.0f / (deltaUV1.x * deltaUV2.y - deltaUV1.y * deltaUV2.x);
 				glm::vec3 tangent = f * (deltaPos1 * deltaUV2.y - deltaPos2 * deltaUV1.y);
				glm::vec3 bitangent = f * (deltaPos2 * deltaUV1.x - deltaPos1 * deltaUV2.x);

				tangents.push_back(tangent);
				tangents.push_back(tangent);
				tangents.push_back(tangent);

				bitangents.push_back(bitangent);
				bitangents.push_back(bitangent);
				bitangents.push_back(bitangent);

		}

		for (unsigned int i=0; i<vertices.size(); i+=1 )
		{		
			glm::vec3 & n = normals[i];
			glm::vec3 & t = tangents[i];
			glm::vec3 & b = bitangents[i];
			
			// Gram-Schmidt orthogonalize
			t = glm::normalize(t - n * glm::dot(n, t));
			
			// Calculate handedness
			if (glm::dot(glm::cross(n, t), b) < 0.0f)
			{
				t = t * -1.0f;
			}
		}

		std::vector<unsigned> indices;
		std::vector<glm::vec3> indexed_vertices;
		std::vector<glm::vec2> indexed_uvs;
		std::vector<glm::vec3> indexed_normals;
		std::vector<glm::vec3> indexed_tangents;
		indexVBO_TBN(	vertices, uvs, normals, tangents, 
						indices, indexed_vertices, indexed_uvs, indexed_normals, indexed_tangents
					);

		m_indicesSize = indices.size();

		glGenBuffers(1, &m_vertexBuffer);
		glBindBuffer(GL_ARRAY_BUFFER, m_vertexBuffer);
		glBufferData(GL_ARRAY_BUFFER, indexed_vertices.size() * sizeof(glm::vec3), &indexed_vertices[0], GL_STATIC_DRAW);

		glGenBuffers(1, &m_normalBuffer);
		glBindBuffer(GL_ARRAY_BUFFER, m_normalBuffer);
		glBufferData(GL_ARRAY_BUFFER, indexed_normals.size() * sizeof(glm::vec3), &indexed_normals[0], GL_STATIC_DRAW);		

		glGenBuffers(1, &m_uvCoords);
		glBindBuffer(GL_ARRAY_BUFFER, m_uvCoords);
		glBufferData(GL_ARRAY_BUFFER, indexed_uvs.size() * sizeof(glm::vec2), &indexed_uvs[0], GL_STATIC_DRAW);	

		glGenBuffers(1, &m_tangents);
		glBindBuffer(GL_ARRAY_BUFFER, m_tangents);
		glBufferData(GL_ARRAY_BUFFER, indexed_tangents.size() * sizeof(glm::vec3), &indexed_tangents[0], GL_STATIC_DRAW);	

		glGenBuffers(1, &m_elementBuffer);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_elementBuffer);
		glBufferData(GL_ELEMENT_ARRAY_BUFFER, indices.size() * sizeof(unsigned), &indices[0], GL_STATIC_DRAW);	
	}

	m_vertexBufferSize = vertices.size();
	m_built = loadStatus;
 }
//----------------------------------------------------------------------
void Model3dCookTorrance::SetTexture(GLuint textureAlbedo, GLuint textureNormal, GLuint textureMetallic, GLuint textureRoughness, 
										GLuint textureAO)
{
	m_textureAlbedo = textureAlbedo;
	m_textureNormal = textureNormal;
	m_textureMetallic = textureMetallic;
	m_textureRoughness = textureRoughness;
	m_textureAO = textureAO;
}
//----------------------------------------------------------------------
// const SceneLightData &sceneLightData, const glm::vec4 &clipPlane
void Model3dCookTorrance::Render(const RenderStateModel3D& renderState, const glm::mat4 &projection, 
						const glm::mat4 &view, const glm::vec3 &pos, const glm::vec3 &rot, const glm::vec3 &scale)
{
	float rotationY = 0.0f;
	glm::mat4 model = glm::mat4(1.0f);
	glm::mat4 rotateBase = glm::rotate(model, glm::radians(rot.x), glm::vec3(1, 0, 0));
	rotateBase = rotateBase * glm::rotate(model, glm::radians(rot.y), glm::vec3(0, 1, 0));
	rotateBase = rotateBase * glm::rotate(model, glm::radians(rot.z), glm::vec3(0, 0, 1));
	glm::mat4 translateBase = glm::translate(model, pos);

	glm::mat4 scaleBase = glm::scale(model, scale);
	model =  translateBase * rotateBase * scaleBase;
	glm::mat4 modelView = view * model;
	glm::mat3 modelView3By3 = glm::mat3(modelView); // top left part of modelView
	glm::mat4 modelViewProjection = projection * modelView;
	glm::mat4 normalMatrix = glm::transpose(glm::inverse(modelView));

	// Forward to shader
	glUniformMatrix4fv(m_model3DCookTorranceShaderHandles.m_modelViewProjectionID, 1, GL_FALSE, &modelViewProjection[0][0]);	
	glUniformMatrix4fv(m_model3DCookTorranceShaderHandles.m_modelMatrixID, 1, GL_FALSE, &model[0][0]);
	glUniformMatrix4fv(m_model3DCookTorranceShaderHandles.m_viewMatrixID, 1, GL_FALSE, &view[0][0]);	
	
	glUniformMatrix4fv(m_model3DCookTorranceShaderHandles.m_modelViewMatrixID, 1, GL_FALSE, &modelView[0][0]);
	glUniformMatrix3fv(m_model3DCookTorranceShaderHandles.m_modelView3By3MatrixID, 1, GL_FALSE, &modelView3By3[0][0]);
	glUniformMatrix4fv(m_model3DCookTorranceShaderHandles.m_normalMatrixID, 1, GL_FALSE, &normalMatrix[0][0]);	

	// Set clip plane (used only when rendering water reflection surface)
	glUniform4f(m_model3DCookTorranceShaderHandles.m_userClipPlane0ID, renderState.m_clipPlane.x, renderState.m_clipPlane.y, 
				renderState.m_clipPlane.z, renderState.m_clipPlane.w);	

	// Light
	glm::vec3 lightPos = renderState.m_sceneLightData.m_lightPos;
	if(renderState.m_invertLightPos)
	{
		lightPos.z = -lightPos.z;
	}

	glUniform3f(m_model3DCookTorranceShaderHandles.m_lightPos_worldspaceID, lightPos.x, lightPos.y, lightPos.z);

	glUniform3f(m_model3DCookTorranceShaderHandles.m_lightColorID, renderState.m_sceneLightData.m_lightColor.x, 
				renderState.m_sceneLightData.m_lightColor.y, renderState.m_sceneLightData.m_lightColor.z);

	glUniform1f(m_model3DCookTorranceShaderHandles.m_lightIntensityID, renderState.m_sceneLightData.m_lightIntensity);
	
	// Bind textures
	glActiveTexture(GL_TEXTURE0);		
	glBindTexture(GL_TEXTURE_2D, m_textureAlbedo);

	glActiveTexture(GL_TEXTURE1);
	glBindTexture(GL_TEXTURE_2D, m_textureNormal);

	glActiveTexture(GL_TEXTURE2);
	glBindTexture(GL_TEXTURE_2D, m_textureMetallic);

	glActiveTexture(GL_TEXTURE3);
	glBindTexture(GL_TEXTURE_2D, m_textureRoughness);
	
	glActiveTexture(GL_TEXTURE4);
	glBindTexture(GL_TEXTURE_2D, m_textureAO);
	
	// Align up with shader
	glUniform1i(m_model3DCookTorranceShaderHandles.m_textureAlbedoID, 0);
	glUniform1i(m_model3DCookTorranceShaderHandles.m_textureNormalID, 1);
	glUniform1i(m_model3DCookTorranceShaderHandles.m_textureMetallicID, 2);
	glUniform1i(m_model3DCookTorranceShaderHandles.m_textureRoughnessID, 3);
	glUniform1i(m_model3DCookTorranceShaderHandles.m_textureAOID, 4);

	// Enable vertex & normal attribute arrays		
	glBindBuffer(GL_ARRAY_BUFFER, m_vertexBuffer);
	glVertexAttribPointer(
		0,
		3,
		GL_FLOAT,
		GL_FALSE,
		0,
		(void*)0
	);

	glBindBuffer(GL_ARRAY_BUFFER, m_normalBuffer);
	glVertexAttribPointer(
		1,
		3,
		GL_FLOAT,
		GL_FALSE,
		0,
		(void*)0
	);

	glBindBuffer(GL_ARRAY_BUFFER, m_uvCoords);
	glVertexAttribPointer(
		2,
		2,
		GL_FLOAT,
		GL_FALSE,
		0,
		(void*)0
	);		

	glBindBuffer(GL_ARRAY_BUFFER, m_tangents);
	glVertexAttribPointer(
		3,
		3,
		GL_FLOAT,
		GL_FALSE,
		0,
		(void*)0
	);

	// Since we are now using indices, bind the index buffer
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_elementBuffer);

	glDrawElements(GL_TRIANGLES, m_indicesSize, GL_UNSIGNED_INT, (void*)0);
}

