// Include standard headers
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <fstream>
#include <vector>
using namespace std;

// Include GLEW
#include <GL/glew.h>

// Include GLFW
#include <GLFW/glfw3.h>

#include "shader.h"
#include "initGL.h"
#include "handleInput.h"
#include "model3d.h"
#include "gameScene.h"
#include "duck.h"
#include "player.h"
#include "projectileBall.h"
#include "collisionDetection.h"
#include "collisionSystem.h"
#include "particleSystem.h"
#include "gui.h"
#include "texturedQuad.h"
#include "texture.h"
#include "text2D.h"
#include "inGameHUD.h"
#include "renderToTexture.h"
#include "gameDefs.h"

#include "aStarPathFind.h"

void DisplayStartupText()
{
	std::string line;
	ifstream startUpText ("startupText.txt");
	if (startUpText.is_open())
	{
		while (getline (startUpText, line) )
		{
			cout << line << endl;
		}
		startUpText.close();
	}	
}

// Spawn a bunch of random ducks
void SpawnDucks(std::vector<Duck*> &ducks, int nrDucks, Player* player)
{
	glm::vec3 playerPos = player->GetPos();
	for(int i = 0; i < nrDucks; i++)
	{
		bool samePos = false;
		float duckPosX = 0.0f;
		float duckPosY = 0.0f;
		
		bool placeLeft = rand() % 2 == 0 ? true : false;
		bool placeTop = rand() % 2 == 0 ? true : false;

		if(placeLeft)
		{
			duckPosX =  playerPos.x - 0.9f - float(rand() % 4);
		}
		else
		{
			duckPosX = playerPos.x + 0.9f + float(rand() % 4);
		}

		if(placeTop)
		{
			duckPosY = playerPos.y + 0.9f + float(rand() % 3);
		}
		else
		{
			duckPosY = playerPos.y - 0.9f - float(rand() % 3);
		}

		Duck* duck = new Duck();	
		
		int duckSize = rand() % 3;
		duckSize = 2;
		duck->SetDuckSize(duckSize + 1);
		if(duckSize == 0)
		{
			duck->SetScale(DUCKSMALL);			
		}
		else if(duckSize == 1)
		{
			duck->SetScale(DUCKMEDIUM);
		}
		else
		{
			duck->SetScale(DUCKBIG);
		}	
		duck->SetPos(glm::vec3(duckPosX, duckPosY, DuckDepth(duckSize)));

		int j = 0;
		while(j < ducks.size())
		{
			Duck* aDuck = ducks[j];
			
			if(aDuck != duck)
			{
				bool collision = SphereSphereTest(duck->GetPos(), duck->GetBoundingSphereRadius(), aDuck->GetPos(), 
									aDuck->GetBoundingSphereRadius());
				if(collision)
				{
					duckPosX += 0.2f;
					duckPosY += 0.2f;

					duckPosX += 0.2f * float(rand() % 10);
					duckPosY += 0.2f * float(rand() % 10);
					duck->SetPos(glm::vec3(duckPosX, duckPosY, DuckDepth(duckSize)));
					j = 0;
					//cout << "fixed a duck" << endl;
				}
				else
				{
					j++;
				}
			}
		}

		duck->SetRot(glm::vec3(90.0f, 0.0f, 0.0f));

		float duckAngle = float(rand() % 360);
		duck->SetFacingRotationImmediate(duckAngle);
		ducks.push_back(duck);
	}

}

void ResetGame(std::vector<std::unique_ptr<Player> > &players, std::vector<Duck* > &ducks, double &timeStartGame, int &nrDucksToCreate)
{
	for(auto &player : players)
	{
		player->ResetHealth();
		player->ResetScore();
		player->SetPos(glm::vec3(0.0f, 0.0f, PLAYERDEPTH));
	}

	for(auto &duck : ducks)
	{
		delete duck;
	}
	ducks.clear();

	nrDucksToCreate = 10;
	SpawnDucks(ducks, nrDucksToCreate, players[0].get());

	timeStartGame = glfwGetTime();
}

bool PlayersDead(const std::vector<std::unique_ptr<Player> > &players)
{
	bool bothPlayersDead = false;
	bool playerOneDead = players[0]->GetHealth() == 0;
	bool playerTwoDead = players.size() == 2 ? players[1]->GetHealth() == 0 : false;
	return playerOneDead && playerTwoDead;
}

bool ShouldUpdateTime(const std::vector<std::unique_ptr<Player> > &players, int minutesLeft, int secondsLeft)
{
	bool bothPlayersDead = PlayersDead(players);

	bool retValue = false;
	if(!bothPlayersDead && !(minutesLeft == 0  && secondsLeft == 0))
	{
		retValue = true;
	}

	return retValue;
}

void GLAPIENTRY
MessageCallback( GLenum source,
                 GLenum type,
                 GLuint id,
                 GLenum severity,
                 GLsizei length,
                 const GLchar* message,
                 const void* userParam )
{
  fprintf( stderr, "GL CALLBACK: %s type = 0x%x, severity = 0x%x, message = %s\n",
           ( type == GL_DEBUG_TYPE_ERROR ? "** GL ERROR **" : "" ),
            type, severity, message );
}

int main( void )
{
	srand(time(NULL)); // Make sure we get new random values from rand

	GLFWwindow* window;

	DisplayStartupText();
	getchar(); // Wait for user to press Enter

	if(InitGL(&window, s_windowWidth, s_windowHeight) == -1)
	{
		return -1;
	}


	//////////// DEBUG OUTPUT ////////////
	/*
		// Enable debug output
		glEnable              ( GL_DEBUG_OUTPUT );
		glDebugMessageCallback( MessageCallback, 0 );
	*/
	//////////////////////////////////////////////////
	

	GLuint vertexArrayID;
	glGenVertexArrays(1, &vertexArrayID);
	glBindVertexArray(vertexArrayID);

	glm::vec3 backgroundColor = glm::vec3(0.0f, 0.41f, 0.66f);

	// Set background color
	glClearColor(backgroundColor.x, backgroundColor.y, backgroundColor.z, 0.0f);		

	InputState inputState;

	Model3d::InitModel3d();
	Duck::InitDucks();
	Text2D::InitText2D();

	// Frame buffer for rendering reflection of water
	GLuint reflectionFrameBuffer, reflectionRenderTexture, reflectionDepthBuffer;
	if(!GenerateFrameBufferAndTexture(512, 512, reflectionFrameBuffer, reflectionRenderTexture, reflectionDepthBuffer))
	{
		cout << "Unable to create frame buffer data for reflections" << endl;
	}
	// TEST RENDER TO BUFFER

	TexturedQuad* testRenderTextureQuad = new TexturedQuad(glm::vec3(450.0f, 450.0f, 0.0f), 300.0f, 300.0f, 
												reflectionRenderTexture);
	

	// END OF TEST


	// PLAYER STUFF
	Player::InitPlayer();
	std::unique_ptr<Player> playerOne = make_unique<Player>(false);
	playerOne->SetPos(glm::vec3(0.0f, 0.0f, PLAYERDEPTH));
	playerOne->SetRot(glm::vec3(90.0f, 0.0f, 0.0f));
	playerOne->SetFacingRotationImmediate(0.0f);
	playerOne->SetColor(glm::vec3(0.7f, 0.0f, 0.0f));
	playerOne->SetScale(0.085f);

	std::unique_ptr<Player> playerTwo = make_unique<Player>(true);
	playerTwo->SetPos(glm::vec3(1.0f, 0.0f, PLAYERDEPTH));
	playerTwo->SetRot(glm::vec3(90.0f, 0.0f, 0.0f));
	playerTwo->SetFacingRotationImmediate(0.0f);
	playerTwo->SetColor(glm::vec3(0.7f, 0.0f, 0.0f));
	playerTwo->SetScale(0.095f);

	std::vector<std::unique_ptr<Player> > players;
	players.push_back(std::move(playerOne));
	//players.push_back(std::move(playerTwo));

	int nrDucksToCreate = 10;
	std::vector<Duck* > ducks;
	SpawnDucks(ducks, nrDucksToCreate, players[0].get());
	

	CollisionEngine &collisionEngine = CollisionEngine::GetInstance(ducks, players);

	ProjectileBall::InitProjectileBall();
	ParticleSystem::InitParticleSystem();
	TexturedQuad::InitTexturedQuad();

	// In game HUD
	InGameHUD gameHUD(true);

	GameScene gameScene(ducks, players, ParticleSystem::GetParticleSystem(), gameHUD.GetGameHud(), s_windowWidth, s_windowHeight);

	// Game timer
	double timeStartGame = glfwGetTime();
	double gameTimeToClearLevel = 90.0f;

	// Restart game
	bool restartGame = false;


	double lastTime = glfwGetTime();
	
	while(glfwGetKey(window, GLFW_KEY_ESCAPE ) != GLFW_PRESS && glfwWindowShouldClose(window) == 0)
	{

		glBindFramebuffer(GL_FRAMEBUFFER, reflectionFrameBuffer);
		glViewport(0,0,512,512);

		glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		// Time difference between frames
		double currentTime = glfwGetTime();
		float deltaTime = float(currentTime - lastTime);

		Duck::CollisionTick(ducks, deltaTime);
		Player::CollisionTick(players, deltaTime);
		collisionEngine.Tick();

		players[0]->Tick(deltaTime);
		if(players.size() == 2)
		{
			players[1]->Tick(deltaTime);
		}

		Duck::Tick(ducks, deltaTime);
		ParticleSystem::TickParticleSystems(deltaTime);
	
		gameHUD.UpdatePlayerHealth(players[0]->GetHealth());
		gameHUD.UpdatePlayerScore(players[0]->GetScore());

		if(players.size() == 2)
		{
			gameHUD.UpdatePlayerTwoHealth(players[1]->GetHealth());
			gameHUD.UpdatePlayerTwoScore(players[1]->GetScore());
		}

		double timePassed = gameTimeToClearLevel - (currentTime - timeStartGame);
		int minutesLeft = static_cast<int>(timePassed / 60);
		int secondsLeft = static_cast<int>(timePassed - static_cast<float>(minutesLeft) * 60.0f);

		if(ShouldUpdateTime(players, minutesLeft, secondsLeft))
		{
			gameHUD.UpdateTimeLeft(minutesLeft, secondsLeft);
		}

		gameHUD.PrepareHUDGUIForRender();

		gameHUD.DebugAddTextureQad(nullptr);
		gameScene.RenderScene();

		glBindFramebuffer(GL_FRAMEBUFFER, 0);
		glViewport(0,0,s_windowWidth,s_windowHeight);
		glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		gameHUD.DebugAddTextureQad(testRenderTextureQuad);

		gameScene.RenderScene();
		
		
		HandleKeyInput(window, inputState, deltaTime, players[0].get(), players.size() == 2 ? players[1].get() : NULL, restartGame);

		glfwPollEvents();
		glfwSwapBuffers(window);

		if(ducks.empty())
		{
			// If one player died, bring him back but give him only 2 lifes at start and reduce his score by 200
			for(auto &player : players)
			{
				if(player->GetHealth() == 0 )
				{
					player->ResetHealth(2);
					player->DecreaseScore(200);
				}
			}

			nrDucksToCreate++;
			SpawnDucks(ducks, nrDucksToCreate, players[0].get());
			
			timeStartGame = glfwGetTime();
		}

		if(PlayersDead(players) || (minutesLeft == 0  && secondsLeft == 0))
		{
			gameHUD.ShowGameOverSign(true);

			if(restartGame)
			{
				ResetGame(players, ducks, timeStartGame, nrDucksToCreate);
				gameHUD.ShowGameOverSign(false);
				restartGame = false;
			}
		}

		lastTime = currentTime;
	}

	ClearTextureCache();

	CleanUpFrameBufferAndAssociatedData(reflectionFrameBuffer, reflectionRenderTexture, reflectionDepthBuffer);

	// Clean up and close OpenGL
	glDeleteVertexArrays(1, &vertexArrayID);	
	glfwTerminate();


	return 0;
}

