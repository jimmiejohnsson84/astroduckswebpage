#ifndef ANIMATED_RECTANGLE_H
#define ANIMATED_RECTANGLE_H

#include <vector>
#include <memory>

// Forward declaration

class TexturedQuad
{
	public:

	TexturedQuad(glm::vec3 start, float width, float height, GLuint texture, float rotation = 0.0f);
	TexturedQuad(glm::vec3 start, float width, float height, std::vector<GLuint> textures, float speedAnimation);	
	~TexturedQuad();

	static void		InitTexturedQuad();

	void		PlayReverse(bool reverse);
	bool		GetPlayReverse() const;
	void 		Tick();
	GLuint		Texture() const;

	static void		Tick(std::vector<std::shared_ptr<TexturedQuad> > &animatedRectangles);

	void		Render(const glm::mat4 &projection, const glm::mat4 &view);

	
	private:

	void			CreateRectanglePositionData();
	void			CreateRectangleUVData();

	void			BuildVBOS();


	glm::vec3 m_start;

	std::vector<glm::vec3>		m_vertices;
	std::vector<glm::vec2>		m_verticesUV;

	std::vector<GLuint> m_textures;
	float m_width;
	float m_height;

	bool	m_hasRotation;
	float	m_rotation;

	bool m_playReverse;
	bool m_firstAnimTime;
	double m_animTime;
	float m_speedAnimation;
	unsigned int m_currentFrame;

	GLuint	m_verticesVBO;
	GLuint	m_verticesUVVBO;

	// Shader data
	
	static GLuint m_programID;
	static GLuint m_mvpID;
	static GLuint m_textureID;	
};

#endif