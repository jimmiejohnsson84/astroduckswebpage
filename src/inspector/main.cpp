// Include standard headers
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <fstream>
#include <vector>

#if defined(_WIN32)
	#include <time.h>
    #include <string>	
#endif

using namespace std;

// Include GLEW
#include <GL/glew.h>

// Include GLFW
#include <GLFW/glfw3.h>

// Include GLM
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
using namespace glm;


#include "../shader.h"
#include "../initGL.h"
#include "../model3d_cooktorrance.h"
#include "../texture.h"
#include "../lightData.h"
#include "../renderStruct.h"
#include "../text2D.h"

void DisplayStartupText()
{
	std::string line;
	ifstream startUpText ("pBRModelInspector_startupText.txt");
	if (startUpText.is_open())
	{
		while (getline (startUpText, line) )
		{
			cout << line << endl;
		}
		startUpText.close();
	}	
}

void TextControls(std::vector<Text2D*> &textControls)
{
	#if defined(_WIN32)
		const float yStart = 0.0f;
		const float textOffsetY = 19.0f;
		const float fontSize = 16.0f;
	#else
		const float yStart = 70.0f;
		const float textOffsetY = 17.0f;
		const float fontSize = 15.0f;
	#endif
	
	textControls.push_back(new Text2D("ESC - QUIT", glm::vec3(20.0f, 													yStart + textOffsetY * 1.0f, 0.0f), fontSize));
	textControls.push_back(new Text2D("PAGEUP,PAGEDOWN - MOVE MODEL BACKWARD, FORWARD", glm::vec3(20.0f, 				yStart + textOffsetY * 2.0f, 0.0f), fontSize));
	textControls.push_back(new Text2D("W,S,A,D,E,R - ROTATES MODEL", glm::vec3(20.0f, 									yStart + textOffsetY * 3.0f, 0.0f), fontSize));
	textControls.push_back(new Text2D("UP,DOWN,LEFT,RIGHT - MOVES MODEL", glm::vec3(20.0f, 								yStart + textOffsetY * 4.0f, 0.0f), fontSize));
	textControls.push_back(new Text2D("SPACE - SWITCH MODEL", glm::vec3(20.0f, 											yStart + textOffsetY * 5.0f, 0.0f), fontSize));
	textControls.push_back(new Text2D("MOUSE - MOVE LIGHT", glm::vec3(20.0f, 											yStart + textOffsetY * 6.0f, 0.0f), fontSize));
	textControls.push_back(new Text2D("SHIFT + MOUSE - MOVE LIGHT FORWARD,BACKWARD", glm::vec3(20.0f, 					yStart + textOffsetY * 7.0f, 0.0f), fontSize));
	textControls.push_back(new Text2D("LEFT MOUSE BUTTON - CHANGE LIGHT COLOR", glm::vec3(20.0f, 						yStart + textOffsetY * 8.0f, 0.0f), fontSize));
	textControls.push_back(new Text2D("+,- CHANGES LIGHT BRIGHTNESS", glm::vec3(20.0f, 									yStart + textOffsetY * 9.0f, 0.0f), fontSize));
}

void HandleInput(GLFWwindow* window, bool &useModel1, int &texturePackage, bool &spaceRel, bool &shiftDown,	bool &mouseLeftButtonRel,
					glm::vec3 &modelPos, glm::vec3 &modelRot, SceneLightData &sceneLight, int &colorLightIndex, float deltaTime)
{
	const float rotSpeed = 60.0f;
	const float moveSpeed = 2.0f;

    #ifdef WIN32
	    const float mouseSpeed  = 1.0f;
    #else
        const float mouseSpeed = 20.0f;
    #endif

	const float lightIntensityChangeSpeed = 25.0f;

	// Move
	if (glfwGetKey(window, GLFW_KEY_UP ) == GLFW_PRESS)
	{
		modelPos.y += deltaTime * moveSpeed;			
	}	

	if (glfwGetKey(window, GLFW_KEY_DOWN ) == GLFW_PRESS)
	{
		modelPos.y -= deltaTime * moveSpeed;		
	}	

	if (glfwGetKey(window, GLFW_KEY_LEFT ) == GLFW_PRESS)
	{
		modelPos.x -= deltaTime * moveSpeed;		
	}
	
	if (glfwGetKey(window, GLFW_KEY_RIGHT ) == GLFW_PRESS)
	{
		modelPos.x += deltaTime * moveSpeed;
	}

	if (glfwGetKey(window, GLFW_KEY_PAGE_UP ) == GLFW_PRESS)
	{
		modelPos.z += deltaTime * moveSpeed;
	}	

	if (glfwGetKey(window, GLFW_KEY_PAGE_DOWN ) == GLFW_PRESS)
	{
		modelPos.z -= deltaTime * moveSpeed;
	}	

	// Rotation
	if (glfwGetKey(window, GLFW_KEY_W ) == GLFW_PRESS)
	{			
		modelRot.x += deltaTime * rotSpeed;
	}	

	if (glfwGetKey(window, GLFW_KEY_S ) == GLFW_PRESS)
	{
		modelRot.x -= deltaTime * rotSpeed;
	}	

	if (glfwGetKey(window, GLFW_KEY_A ) == GLFW_PRESS)
	{
		modelRot.y -= deltaTime * rotSpeed;
	}
	
	if (glfwGetKey(window, GLFW_KEY_D ) == GLFW_PRESS)
	{
		modelRot.y += deltaTime * rotSpeed;
	}

	if (glfwGetKey(window, GLFW_KEY_E ) == GLFW_PRESS)
	{
		modelRot.z += deltaTime * rotSpeed;
	}

	if (glfwGetKey(window, GLFW_KEY_R ) == GLFW_PRESS)
	{
		modelRot.z -= deltaTime * rotSpeed;
	}

	// Model switch key
	if (spaceRel && glfwGetKey(window, GLFW_KEY_SPACE ) == GLFW_PRESS)
	{
		spaceRel = false;

		if(useModel1)
		{
			useModel1 = false;
		}
		else
		{
			if(texturePackage == 3)
			{
				useModel1 = true;
				texturePackage = 0;
			}
			else
			{
				texturePackage++;
			}				
		}			
	}

	// Increase/Decrease light strength
	if (glfwGetKey(window, GLFW_KEY_KP_ADD ) == GLFW_PRESS)
	{
		sceneLight.m_lightIntensity += deltaTime * lightIntensityChangeSpeed;

		if(sceneLight.m_lightIntensity > 1000.0f)
		{
			sceneLight.m_lightIntensity = 1000.0f;
		}
	}

	// Switch light color
	if (glfwGetMouseButton(window, GLFW_MOUSE_BUTTON_LEFT ) == GLFW_PRESS && mouseLeftButtonRel)
	{
		mouseLeftButtonRel = false;

		switch(colorLightIndex)
		{
			// White -> Red
			case 0:
				sceneLight.m_lightColor = glm::vec3(1.0, 0.0f, 0.0f);
				colorLightIndex++;
			break;


			// Red -> Green
			case 1:
				sceneLight.m_lightColor = glm::vec3(0.0, 1.0f, 0.0f);
				colorLightIndex++;
			break;

			// Green -> Blue
			case 2:
				sceneLight.m_lightColor = glm::vec3(0.0, 0.0f, 1.0f);
				colorLightIndex++;
			break;

			// Blue -> White
			case 3:
				sceneLight.m_lightColor = glm::vec3(1.0, 1.0f, 1.0f);
				colorLightIndex = 0;
			break;
		}
	}

	if (glfwGetKey(window, GLFW_KEY_KP_SUBTRACT ) == GLFW_PRESS)
	{
		sceneLight.m_lightIntensity -= deltaTime * lightIntensityChangeSpeed;

		if(sceneLight.m_lightIntensity < 0.0f)
		{
			sceneLight.m_lightIntensity = 0.0f;
		}
	}


	if (glfwGetKey(window, GLFW_KEY_SPACE ) != GLFW_PRESS)
	{
		spaceRel = true;
	}

	if (glfwGetKey(window, GLFW_KEY_LEFT_SHIFT ) == GLFW_PRESS || glfwGetKey(window, GLFW_KEY_RIGHT_SHIFT ) == GLFW_PRESS)
	{
		shiftDown = true;
	}

	if (glfwGetMouseButton(window, GLFW_MOUSE_BUTTON_LEFT ) != GLFW_PRESS)
	{
		mouseLeftButtonRel = true;
	}

	// Handle mouse input
	double xpos, ypos;
	glfwGetCursorPos(window, &xpos, &ypos);	
	glfwSetCursorPos(window, s_windowWidth / 2, s_windowHeight / 2);

	float deltaMouseX = (xpos - (static_cast<double>(s_windowWidth) / 2.0));
	float deltaMouseY = ((static_cast<double>(s_windowHeight) / 2.0) - ypos);

	if(fabs(deltaMouseX) > 300.0f || fabs(deltaMouseY) > 300.0f )
	{
		deltaMouseX = 0.0f;
		deltaMouseY = 0.0f;
	}

	sceneLight.m_lightPos.x += deltaMouseX * deltaTime * mouseSpeed;

	if(shiftDown)
	{
		sceneLight.m_lightPos.z -= deltaMouseY * deltaTime * mouseSpeed;
	}
	else
	{
		sceneLight.m_lightPos.y += deltaMouseY * deltaTime * mouseSpeed;
	}
}

int main( void )
{
	GLFWwindow* window;

	DisplayStartupText();
	getchar(); // Wait for user to press Enter

	if(InitGL(&window, s_windowWidth, s_windowHeight) == -1)
	{
		return -1;
	}

 	// Hide the mouse and enable unlimited mouvement
    glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);	

	GLuint vertexArrayID;
	glGenVertexArrays(1, &vertexArrayID);
	glBindVertexArray(vertexArrayID);

	Model3dCookTorrance::InitModel3dCookTorrance();
	Text2D::InitText2D();

	std::vector<Text2D*> textControls; 
	TextControls(textControls);

	double lastTime = glfwGetTime();
	
	Model3dCookTorrance* m_modelCookTorrance = new Model3dCookTorrance();
	m_modelCookTorrance->Build("boat.obj");
	m_modelCookTorrance->SetTexture(GetTextureResource(FISHINGBOAT_ALBEDO_TEXTURE), GetTextureResource(FISHINGBOAT_NORMAL_TEXTURE),
									GetTextureResource(FISHINGBOAT_METALLIC_TEXTURE), GetTextureResource(FISHINGBOAT_ROUGHNESS_TEXTURE),
									GetTextureResource(FISHINGBOAT_AO_TEXTURE));


	Model3dCookTorrance* m_modelCookTorrance2 = new Model3dCookTorrance();
	m_modelCookTorrance2->Build("rubberDuck.obj");
	m_modelCookTorrance2->SetTexture(GetTextureResource(DUCK_PLASTIC_ALBEDO_TEXTURE), GetTextureResource(DUCK_PLASTIC_NORMAL_TEXTURE),
									GetTextureResource(DUCK_PLASTIC_METALLIC_TEXTURE), GetTextureResource(DUCK_PLASTIC_ROUGHNESS_TEXTURE),
									GetTextureResource(DUCK_PLASTIC_AO_TEXTURE));


	Model3dCookTorrance* m_lightSource = new Model3dCookTorrance();
	m_lightSource->Build("ball.obj");
	m_lightSource->SetTexture(GetTextureResource(PROJECTILE_BALL_ALBEDO_WHITE_TEXTURE), GetTextureResource(PROJECTILE_BALL_NORMAL_TEXTURE),
									GetTextureResource(PROJECTILE_BALL_METALLIC_TEXTURE), GetTextureResource(PROJECTILE_BALL_ROUGHNESS_TEXTURE),
									GetTextureResource(PROJECTILE_BALL_AO_TEXTURE));
	const glm::vec3 lightSourceRot(0.0f, 0.0f, 0.0f);
	const glm::vec3 lightSourceScale(0.1f, 0.1f, 0.1f);
	int colorLightIndex  = 0; // Just a simple way to switch between 4 different colors - white, red, gren and blue

	glm::vec3 modelPos = glm::vec3(0.0f, -1.0f, -10.0f);
	glm::vec3 modelRot = glm::vec3(0.0f, 0.0f,  0.0f);
	glm::vec3 modelScale = glm::vec3(1.0f, 1.0f,  1.0f);

	glm::vec3 modelScaleDuck = glm::vec3(15.0f, 15.0f,  15.0f);

	// Basal stuff needed for rendering
	const float fov = 45.0f;
	float nearZ = 0.1f;
	float farZ = 100.0f;
	glm::mat4 projection = glm::perspective(glm::radians(fov), (float)4.0f / (float)3.0f, nearZ, farZ);	
	glm::mat4 textMVP = glm::ortho(0.0f, static_cast<float>(s_windowWidth), static_cast<float>(s_windowHeight), 0.0f);
	glm::mat4 view; // Just identity matrix
	SceneLightData sceneLight;
	InitLightData(sceneLight);

	sceneLight.m_lightPos.x = 1.0f;
	sceneLight.m_lightPos.y = 1.0f;
	sceneLight.m_lightPos.z = -5.0f;

	glm::vec4 clipPlane = glm::vec4(0.0f, 0.0f, 1.0f, farZ);
	const RenderStateModel3D model3DRenderState = {sceneLight, clipPlane, false};

	bool useModel1 = true;
	int texturePackage = 0;
	bool spaceRel = true;

	bool shiftDown = false;
	bool mouseLeftButtonRel = true;

	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
	while(glfwGetKey(window, GLFW_KEY_ESCAPE ) != GLFW_PRESS && glfwWindowShouldClose(window) == 0)
	{
		glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		// Time difference between frames
		double currentTime = glfwGetTime();
		float deltaTime = float(currentTime - lastTime);
		lastTime = currentTime;

		// Handle key input
		shiftDown = false;
		glfwPollEvents();

		HandleInput(window, useModel1, texturePackage, spaceRel, shiftDown, mouseLeftButtonRel, modelPos, modelRot, sceneLight, colorLightIndex, deltaTime);

		Model3dCookTorrance::PrepareRender();
		if(useModel1)
		{
			m_modelCookTorrance->Render(model3DRenderState, projection, view, modelPos, modelRot, modelScale);
		}
		else
		{
			switch(texturePackage)
			{
				case 0:
					m_modelCookTorrance2->SetTexture(GetTextureResource(DUCK_PLASTIC_ALBEDO_TEXTURE), GetTextureResource(DUCK_PLASTIC_NORMAL_TEXTURE),
													GetTextureResource(DUCK_PLASTIC_METALLIC_TEXTURE), GetTextureResource(DUCK_PLASTIC_ROUGHNESS_TEXTURE),
													GetTextureResource(DUCK_PLASTIC_AO_TEXTURE));
				break;

				case 1:
					m_modelCookTorrance2->SetTexture(GetTextureResource(DUCK_WOOD_ALBEDO_TEXTURE), GetTextureResource(DUCK_PLASTIC_NORMAL_TEXTURE),
													GetTextureResource(DUCK_WOOD_METALLIC_TEXTURE), GetTextureResource(DUCK_WOOD_ROUGHNESS_TEXTURE),
													GetTextureResource(DUCK_WOOD_AO_TEXTURE));
				break;

				case 2:
					m_modelCookTorrance2->SetTexture(GetTextureResource(DUCK_METAL_ALBEDO_TEXTURE), GetTextureResource(DUCK_PLASTIC_NORMAL_TEXTURE),
													GetTextureResource(DUCK_METAL_METALLIC_TEXTURE), GetTextureResource(DUCK_METAL_ROUGHNESS_TEXTURE),
													GetTextureResource(DUCK_METAL_AO_TEXTURE));
				break;

				case 3:
					m_modelCookTorrance2->SetTexture(GetTextureResource(DUCK_GOLD_ALBEDO_TEXTURE), GetTextureResource(DUCK_PLASTIC_NORMAL_TEXTURE),
													GetTextureResource(DUCK_GOLD_METALLIC_TEXTURE), GetTextureResource(DUCK_GOLD_ROUGHNESS_TEXTURE),
													GetTextureResource(DUCK_GOLD_AO_TEXTURE));
				break;

			}
			m_modelCookTorrance2->Render(model3DRenderState, projection, view, modelPos, modelRot, modelScaleDuck);
		}

		m_lightSource->Render(model3DRenderState, projection, view, sceneLight.m_lightPos, lightSourceRot, lightSourceScale);
		
		Model3dCookTorrance::AfterRender();

		for(auto text : textControls)
		{
			text->Render(textMVP, view);
		}

		glfwSwapBuffers(window);
	}

	ClearTextureCache();

	delete m_modelCookTorrance;
	delete m_modelCookTorrance2;
	delete m_lightSource;

	for(auto text : textControls)
	{
		delete text;
	}
	// Clean up and close OpenGL
	glDeleteVertexArrays(1, &vertexArrayID);	
	glfwTerminate();

	return 0;
}

