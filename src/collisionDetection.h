#ifndef COLLISIONDETECTION_H
#define COLLISIONDETECTION_H

#include <vector>
#include <memory>
#include <glm/glm.hpp>

bool SphereSphereTest(const glm::vec3 &s1Pos, float s1Radius, const glm::vec3 &s2Pos, float s2Radius);
bool SphereBorderTest(const glm::vec3 &sphere, float sphereRadius, float xMin, float yMin, float xMax, float yMax);

#endif