   _____            __               ________                 __            
  /  _  \   _______/  |________  ____\______ \  __ __   ____ |  | __  ______
 /  /_\  \ /  ___/\   __\_  __ \/  _ \|    |  \|  |  \_/ ___\|  |/ / /  ___/
/    |    \\___ \  |  |  |  | \(  <_> )    `   \  |  /\  \___|    <  \___ \ 
\____|__  /____  > |__|  |__|   \____/_______  /____/  \___  >__|_ \/____  >
        \/     \/                            \/            \/     \/     \/ 
                                                                            

Shoot all the ducks before the time runs out
Extra life is given once you hit a certain score point

*** Controls ***
Up/Down, Left/Right - control player
Spacebar - shoot
Esc - Quit game
*** Controls ***

Credits:
- Jimmie Johnsson, Check out http://jimmiejohnsson84.me/ to see all the source code and a detailed tutorial on how to make this game!

One texture (drainage at bottom of pool) is from:
http://opengameart.org (https://opengameart.org/users/leeornet)

Thank you for playing and feel free to support the game and me by paying for the game!
Press enter to start playing