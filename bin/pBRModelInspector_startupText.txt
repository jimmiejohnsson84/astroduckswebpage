    ____  ____  ____        __  ___          __     __   ____                           __            
   / __ \/ __ )/ __ \      /  |/  /___  ____/ /__  / /  /  _/___  _________  ___  _____/ /_____  _____
  / /_/ / __  / /_/ /_____/ /|_/ / __ \/ __  / _ \/ /   / // __ \/ ___/ __ \/ _ \/ ___/ __/ __ \/ ___/
 / ____/ /_/ / _, _/_____/ /  / / /_/ / /_/ /  __/ /  _/ // / / (__  ) /_/ /  __/ /__/ /_/ /_/ / /    
/_/   /_____/_/ |_|     /_/  /_/\____/\__,_/\___/_/  /___/_/ /_/____/ .___/\___/\___/\__/\____/_/     
                                                                   /_/                                

Allows you to visually inspect a 3D model

*** Controls ***
Esc - Quit
Up/Down, Left/Right - move model 
Page up/Page down - zoom in out
W/S - Rotate around X axis
A/D - Rotate around Y axis
E/R - Rotate around Z axis
Space - Switch model/texture pack
Mouse - move light (X-, Y-axis)
Shift + mouse - move light forward, back
Left mouse button - change light color
-/+ - increase/decrease light brightness
*** Controls ***

Credits:
- Jimmie Johnsson, Check out http://jimmiejohnsson84.me/ to see all the source code and a detailed tutorial on how to make this game!

Thank you for testing and feel free to support my project by buying AstroDucks on itch!
Press enter to start