AstroDucks
========================

```bash
To build:
make dir build
step into build
cmake ..
make astroDucks


To run:
unzip textures.zip in the folder it is located in.
run astroDucks
```

Physically Based Rendering - Model Inspector
=========================

```bash
To build:
make dir build
step into build
cmake ..
make pbrMdlInspector

To run:
unzip textures.zip in the folder it is located in.
run pbrMdlInspector
```